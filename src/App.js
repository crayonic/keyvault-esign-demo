import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import RefreshIcon from '@material-ui/icons/Refresh';
import Typography from '@material-ui/core/Typography';
import { blue } from '@material-ui/core/colors';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import Tooltip from '@material-ui/core/Tooltip';

import { Certificate } from '@fidm/x509';

import { Certificates as KeyVaultCertificates } from "keyvault-browser-js"

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function SelectCertificate(props) {
  const classes = useStyles();
  const { onClose, open, certificates, onRefreshCertificatesCache } = props;

  const handleClose = () => {
    onClose();
  };

  const handleListItemClick = (value) => {
    onClose(value);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="simple-dialog-title">Choose certificate</DialogTitle>
      <List>
        {certificates.map((certificate) => (
          // <Tooltip title={certificate.displayDescription} placement="left" key={certificate.certId}>
            <ListItem button onClick={() => handleListItemClick(certificate)} key={certificate.certId}>
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  <PersonIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={certificate.displayName} secondary={certificate.displayDescription} />
            </ListItem>
          // </Tooltip>
        ))}
        <ListItem button onClick={onRefreshCertificatesCache}>
            <ListItemAvatar>
              <Avatar className={classes.avatar}>
                <RefreshIcon />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={"Refresh certificates cache"} />
          </ListItem>
      </List>
    </Dialog>
  );
}

// SimpleDialog.propTypes = {
//   onClose: PropTypes.func.isRequired,
//   open: PropTypes.bool.isRequired,
//   selectedValue: PropTypes.string.isRequired,
// };

function App() {
  const classes = useStyles();
  const [certificates, setCertificates] = useState([]);
  const [documentSignature, setDocumentSignature] = useState(null);
  const [showCertificateSelector, setShowCertificateSelector] = useState(false);

  const [showProgress, setShowProgress] = React.useState(false);
  const [progressMessage, setProgressMessage] = React.useState("");

  const [showError, setShowError] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");

  const handleClickSign = async (forceRefresh) => {
      setProgressMessage("Reading available certificates");
      setShowProgress(true);
      try {
        if (forceRefresh || certificates.length <= 0) {
          const certs = await KeyVaultCertificates.getCertificates();
          for (const certificate of certs) {
            const decoded = Certificate.fromPEM(Buffer.from(certificate.pemData, 'utf8'));
            certificate.decoded = decoded;
            certificate.displayName = `${decoded.subject.commonName} (${decoded.issuer.commonName})`;
            let displayDescription = `PIV slot ${certificate.pivSlot}; Serial number: ${decoded.serialNumber}; ${decoded.publicKey.algo}`;
            for (const extension of decoded.extensions) {
              if (extension.name === "keyUsage") {
                displayDescription += "; KU: " +
                  Object.keys(extension)
                  .filter(key => !["oid", "name", "keyUsage", "value"].includes(key))
                  .filter(key => extension[key])
                  .join(", ")
              }
              if (extension.name === "extKeyUsage") {
                displayDescription += "; EKU: " +
                  Object.keys(extension)
                  .filter(key => !["oid", "name", "value"].includes(key))
                  .filter(key => extension[key])
                  .join(", ")
              }
            }
            certificate.displayDescription = displayDescription;
            console.log(certificate);
          }
          setCertificates(certs);
        }
        setShowCertificateSelector(true);
      } catch(e) {
        console.warn("handleClickSign", e);
        setErrorMessage(e.message);
        setShowError(true);
      }
      setShowProgress(false);
  };

  const handleSelectedCertificate = async (selectedCertificate) => {
    setShowCertificateSelector(false);
    if (selectedCertificate) {
      setShowProgress(true);
      setProgressMessage(`Signing with ${selectedCertificate.displayName}`);
      try {
        setDocumentSignature(await KeyVaultCertificates.signHash(selectedCertificate.certId, "hash8946511891548"));
      } catch(e) {
        console.warn("sign hash", e);
        setErrorMessage(e.message);
        setShowError(true);
      }
      setShowProgress(false);
    }
  };

  const handleRefreshCertificatesCache = () => {
    setShowCertificateSelector(false);
    handleClickSign(true);
  }
  
  return (
    <div>
      <Snackbar open={showError} autoHideDuration={6000} onClose={() => setShowError(false)} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
        <Alert severity="error" onClose={() => setShowError(false)}>
          {errorMessage}
        </Alert>
      </Snackbar>
      <Backdrop className={classes.backdrop} open={showProgress}>
        <CircularProgress color="inherit" />
        &nbsp;
        <Typography variant="inherit">{progressMessage}</Typography>
      </Backdrop>
      <Button 
        color="primary"
        onClick={handleClickSign} 
      >
        Sign document with KeyVault
      </Button>
      <SelectCertificate certificates={certificates} open={showCertificateSelector} onClose={handleSelectedCertificate} onRefreshCertificatesCache={handleRefreshCertificatesCache} />
      <Typography variant="subtitle1">Document signature: {JSON.stringify(documentSignature)}</Typography>
    </div>
  );
}
export default App;
